### 散列函数
- 1、把字符串中字符的ascii码值加起来
```c
Index Hash(const char *Key, int TableSize)
{
    unsigned int HashVal = 0;

    while (*Key != '\0') {
        HashVal += *Key++;
    }

    return HashVal % TableSize;
}
```

- 2、根据Horner法则计算一个多项式函数
```c
Index Hash(const char *Key, int TableSize)
{
    unsigned int HashVal = 0;

    while (*Key != '\0') {
        HashVal = (HashVal << 5) + *Key++;
    }

    return HashVal % TableSize;
}
```
