# PHP 知识

## 类型
- Boolean 类型
- Integer 整型
- Float 浮点型
- String 字符串
- Array 数组
- Object 对象
- Resource 资源类型
- NULL
- Callback / Callable 类型

## 类与对象
- 类的自动加载

## 数据对象
- PDO
- PDOStatement

## 垃圾回收机制
- 垃圾收集器
- 垃圾回收算法

## PHP框架
- Laravel
- Yii2
- ThinkPHP5
- Yaf